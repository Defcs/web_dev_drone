import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Импортируйте файлы CSS Bootstrap и BootstrapVue (порядок важен)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Сделайте BootstrapVue доступным для всего проекта
Vue.use(BootstrapVue)
// При желании установите плагин компонентов иконок BootstrapVue
Vue.use(IconsPlugin)

Vue.use(VueRouter);
Vue.use(Vuex);

Vue.config.productionTip = false

import mainpage from "@/pages/MainPage"
import catalogpage from "@/pages/CatalogPage"
import catalogsecpage from "@/pages/CatalogSecCatigories"
import catalogthirdpage from "@/pages/CatalogThirdCatigories"
import testpage from "@/pages/TestingPage"
import productpage from "@/pages/ProductPage"

const routes = [
  { path: '/', component: mainpage },
  { path: '/catalog', component: catalogpage },
  { path: '/catalog/:link', component: catalogsecpage },
  { path: '/catalog/:link/:linktwo', component: catalogthirdpage },
  { path: '/catalog/:link/:linktwo/:product', component: productpage},
  { path: '/test', component: testpage },
]

const router = new VueRouter({
  routes 
})

import {store} from '@/store/store'

const storeObj = new Vuex.Store(store);

new Vue({
  store:storeObj,
  router,
  render: h => h(App),
}).$mount('#app')
