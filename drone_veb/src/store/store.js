import axios from "axios";

export const store = {
    state:{
        catalog:[],
        comments: [],
        path:[],
    },
    getters:{
        getCatalog: state =>{
            return state.catalog
        },
        getSecCatalog: (state) => (take_link) =>{
            // console.log('state.catalog',state.catalog[0])
            // console.log('take_link',take_link)
            // console.log('state.catalog.find((element)=>element.link==take_link)',state.catalog.find((element)=>element.link==take_link))
            return state.catalog.find((element)=>element.link==take_link)
        },
        getThirdCatalog: (state) => (take_link,take_link_sec) =>{
            let res
            res = state.catalog.find((element)=>element.link==take_link).catalog_sec.find((element)=>element.link==take_link_sec)
            // console.log(res.title)
            return res
        },
        getPath: state =>{
            return state.path
        },
    },
    mutations:{
        setCatalog(state,catalog){
            state.catalog = catalog;
            // console.log("suc",catalog)
        },
        setPath(state,path){
            state.path = path
        }
    },
    actions:{
        async loadCatalog(context){
            const {data} = await axios.get('http://localhost:8000/catalog_mini');

            // console.log(data.json[0].popularcats);
            // console.log('data',data);
            
            if (data){
                context.commit('setCatalog',data);
            }
        },
    }
}