import redis
import datetime  
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token


def logs_handler(request):
    # print('csrf',request.headers['X-CSRFToken'])
    # print('path',request.get_full_path)
    # print('PWD',request.META['HTTP_AUTHORIZATION'][6:])
    # print('sdgjiesgjiodgs',Token.objects.get(key = request.META['HTTP_AUTHORIZATION'][6:]).user_id)
    if request.get_full_path() == '/registration' and request.POST['username']:
        return {
            'path':request.get_full_path(),
            'user':None,
            'time':datetime.datetime.now(),
            'request_data':request.POST['username'],
            'method':request.method,
            'agent':request.META['HTTP_USER_AGENT'],
        }
    if request.get_full_path() == '/authorisation' and request.POST['username']:
        return {
            'path':request.get_full_path(),
            'user':User.objects.get(username = request.POST['username']),
            'time':datetime.datetime.now(),
            'request_data':request.POST['username'],
            'method':request.method,
            'agent':request.META['HTTP_USER_AGENT'],
        }
    try:
        if request.method == 'POST' and request.META['HTTP_AUTHORIZATION'][6:]:
            return {
                'path':request.get_full_path(),
                'user':Token.objects.get(key = request.META['HTTP_AUTHORIZATION'][6:]).user,
                'time':datetime.datetime.now(),
                'request_data':request.POST,
                'method':request.method,
                'agent':request.META['HTTP_USER_AGENT'],
        }
    except:
        try:
            if request.META['HTTP_AUTHORIZATION'][6:]:
                return {
                    'path':request.get_full_path(),
                    'user':Token.objects.get(key = request.META['HTTP_AUTHORIZATION'][6:]).user,
                    'time':datetime.datetime.now(),
                    'request_data':None,
                    'method':request.method,
                    'agent':request.META['HTTP_USER_AGENT'],
            }
        except:
            return None
    try:
        if request.META['HTTP_AUTHORIZATION'][6:]:
            return {
                'path':request.get_full_path(),
                'user':Token.objects.get(key = request.META['HTTP_AUTHORIZATION'][6:]).user,
                'time':datetime.datetime.now(),
                'request_data':None,
                'method':request.method,
                'agent':request.META['HTTP_USER_AGENT'],
            }
    except:
        return None