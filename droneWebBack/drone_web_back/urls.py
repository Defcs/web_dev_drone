"""drone_web_back URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.contrib.auth import update_session_auth_hash
# from fitnesssite.controllers import views,Auth_Controller,Schedule_Controller
from django.urls import include, path 
from django.conf.urls.static import static
from django.conf import settings
from rest_framework import routers
from main_functional import views
from django.urls import re_path
from rest_framework_swagger.views import get_swagger_view
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view
from rest_framework import permissions

# router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)
# router.register(r'groups', views.GroupViewSet) 
# # router.register(r'catalog', views.CatalogViewSet) 
# router.register(r'catalogsec', views.CatalogSecViewSet)
schema_view = get_swagger_view(title='Pastebin API')

urlpatterns = [
    path('api_schema/', get_schema_view(
        title='API Schema',
        description='Guide for the REST API',
        permission_classes=(permissions.AllowAny,),
    ), name='api_schema',),
    re_path(r'^auth/', include('drf_social_oauth2.urls', namespace='drf')),
    path('admin/', admin.site.urls),
    # path('', include('fitnesssite.urls')),
    path('', include('main_functional.urls')),
    # path('api/', include(router.urls)),
    # path('api/trainings/', views.TrainingsViewSet.as_view()),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
