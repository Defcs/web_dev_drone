from django.contrib.auth.models import User, Group
from main_functional.models import more_about_user,catalog_first_lvl,catalog_sec_lvl,comments,reviews,good
from rest_framework import serializers

class More_about_user_dataSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    date_birth = serializers.CharField()
    gender = serializers.CharField()
    real_name = serializers.CharField()
    real_sename = serializers.CharField()
    token = serializers.CharField()

class User_dataSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    more_about_users = More_about_user_dataSerializer(many = False,read_only=True)
    username = serializers.CharField(read_only=True)

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return User.objects.create(**validated_data)

    def get_info_status(self):
        print(id)

