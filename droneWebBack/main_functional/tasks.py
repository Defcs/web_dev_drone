from time import sleep
from django.core.mail import send_mail
from celery import shared_task
import redis
from django.conf import settings
from main_functional.resources import redis_connector 
from django.contrib.auth.models import User
from django.core.cache import cache

@shared_task(name='send_message_on_registrate')
def send_feedback_email_task(email_address, message):
    # sleep(20) 
    print('YESS')
    send_mail(
        "Your Feedback",
        f"\t{message}\n\nThank you!",
        "support@example.com",
        [email_address],
        fail_silently=False,
    )

# @shared_task(name='send_message_report')
# def send_feedback_email_task(email_address='support@example.com', message='На данный момент нет статистики'):
#     # sleep(20) 
#     print('YESS')
#     send_mail(
#         "Отчет",
#         f"\t{message}\n\nКонец отчета",
#         "support@example.com",
#         [email_address],
#         fail_silently=False,
#     )


@shared_task(name='saving_casche')
def save_casche_in_db():
    print('BBBBBBBBBBBBBB')
    result = redis_connector.get_from_casche()
    print('result',result)
    redis_connector.save_cashe_into_db(result)
    return 'result'