import datetime
from django.db import models
from django.utils import timezone
from django.contrib import admin

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
# Create your models here.

class more_about_user(models.Model):
    id = models.AutoField(primary_key=True,auto_created=True)
    user = models.OneToOneField('auth.user',on_delete=models.CASCADE,verbose_name = 'Имя пользователя',related_name='more_about_users')
    date_birth = models.DateTimeField(verbose_name = 'Дата рождения',blank=True,null=True)
    gender = models.CharField(max_length=200,verbose_name = 'Пол',default='Неизвестно',blank=True)
    real_name = models.CharField(max_length=200,verbose_name = 'Имя',default='Неизвестно',blank=True)
    real_sename = models.CharField(max_length=200,verbose_name = 'Фамилия',default='Неизвестно',blank=True)
    token = models.CharField(max_length=12,verbose_name = 'Фамилия',default='Неизвестно',blank=True,null=True)
    # def age(self):
    #     return date.today().year - self.date_birth.year

    class Meta:
        verbose_name = 'Дополнительно о пользователе'
        verbose_name_plural = 'Дополнительно о пользователях'
    
    @admin.display(
        boolean=True,
        ordering='user',
    )
    def __str__(self):
        return '{} {} {} {}'.format(self.id,self.user.username,self.date_birth,self.gender)

class catalog_first_lvl(models.Model):
    id = models.AutoField(primary_key=True,auto_created=True)
    title = models.CharField(max_length=200,verbose_name = 'Заголовок категории')
    img = models.ImageField(upload_to="catalog_first_lvl_img/",verbose_name = 'Изображение категории',blank=True)
    link = models.CharField(max_length=200,verbose_name = 'ТИП категории')

    class Meta:
        verbose_name = 'Каталог первого уровня'
        verbose_name_plural = 'Каталоги первого уровня'
    
    @admin.display(
        boolean=True,
        ordering='id',
    )
    def __str__(self):
        return '{} {} {} {}'.format(self.id,self.title,self.img,self.link)

class catalog_sec_lvl(models.Model):
    id = models.AutoField(primary_key=True,auto_created=True)
    title = models.CharField(max_length=200,verbose_name = 'Заголовок категории')
    img = models.ImageField(upload_to="catalog_sec_lvl_img/",verbose_name = 'Изображение категории',blank=True)
    link = models.CharField(max_length=200,verbose_name = 'ТИП категории')
    catalog_first = models.ForeignKey('catalog_first_lvl',on_delete=models.CASCADE,verbose_name = 'Каталог первого уровня',related_name='catalog_sec') 

    class Meta:
        verbose_name = 'Каталог второго уровня'
        verbose_name_plural = 'Каталоги второго уровня'
    
    @admin.display(
        boolean=True,
        ordering='id',
    )
    def __str__(self):
        return '{} {} {} {} {}'.format(self.id,self.title,self.img,self.link,self.catalog_first.link)

class comments(models.Model):
    id = models.AutoField(primary_key=True,auto_created=True)
    title = models.CharField(max_length=200,verbose_name = 'Заголовок комментария')
    desc = models.CharField(max_length=2000,verbose_name = 'Описание комментария')
    stars = models.CharField(max_length=1,verbose_name = 'Звезды')
    user = models.ForeignKey('auth.user',on_delete=models.CASCADE,verbose_name = 'Имя комментатора',related_name='commenter')
    tw = models.ForeignKey('good',on_delete=models.CASCADE,verbose_name = 'Товар',related_name='smth')

    class Meta:
        verbose_name = 'Комментарий пользователя'
        verbose_name_plural = 'Комментарии пользователей'
    
    @admin.display(
        boolean=True,
        ordering='id',
    )
    def __str__(self):
        return '{} {} {} {} {} {} {}'.format(self.id,self.title,self.desc,self.stars,self.user.username,self.user.id,self.tw.id)

class reviews(models.Model):
    id = models.AutoField(primary_key=True,auto_created=True)
    title = models.CharField(max_length=200,verbose_name = 'Заголовок обзора')
    desc = models.CharField(max_length=20000,verbose_name = 'Описание обзора')
    stars = models.CharField(max_length=1,verbose_name = 'Звезды')
    user = models.ForeignKey('auth.user',on_delete=models.CASCADE,verbose_name = 'Имя обозревателя',related_name='reviewer')
    tw = models.ForeignKey('good',on_delete=models.CASCADE,verbose_name = 'Товар',related_name='smthing')

    class Meta:
        verbose_name = 'Обзоры пользователя'
        verbose_name_plural = 'Обзоры пользователей'
    
    @admin.display(
        boolean=True,
        ordering='id',
    )
    def __str__(self):
        return '{} {} {} {} {} {} {}'.format(self.id,self.title,self.desc,self.stars,self.user.username,self.user.id,self.tw.id)

class good(models.Model):
    id = models.AutoField(primary_key=True,auto_created=True)
    title = models.CharField(max_length=200,verbose_name = 'Заголовок товара')
    desc = models.CharField(max_length=2000,verbose_name = 'Описание товара')
    charact = models.CharField(max_length=2000,verbose_name = 'Характеристики товара')
    count = models.CharField(max_length=5,verbose_name = 'Количество товара товара')
    customer = models.ManyToManyField('auth.user',verbose_name = 'Имя покупателя',related_name='customer',blank=True)
    img = models.ImageField(upload_to="good_img/",verbose_name = 'Изображение товара',blank=True)
    link = models.CharField(max_length=300,verbose_name = 'Полное наимаенование товара')
    stars = models.CharField(max_length=1,verbose_name = 'Звезды')
    new_price = models.CharField(max_length=7,verbose_name = 'Новая цена')
    old_price = models.CharField(max_length=7,verbose_name = 'Старая цена')
    material = models.CharField(max_length=5,verbose_name = 'Материал')
    speed = models.CharField(max_length=5,verbose_name = 'Скорость')
    fly_time = models.CharField(max_length=5,verbose_name = 'Время полета')
    sec_lvl = models.ForeignKey('catalog_sec_lvl',on_delete=models.CASCADE,verbose_name = 'Каталог 2 лвлва',related_name='goods_cat')

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
    
    @admin.display(
        boolean=True,
        ordering='id',
    )
    def __str__(self):
        return '{} {} {} {} {} {} {} {} {} {} {} {} {} {}'.format(self.id,self.title,self.desc,self.charact,self.count,self.customer,self.img,self.link,self.stars,self.new_price,self.old_price,self.material,self.speed,self.fly_time)

class more_good_images(models.Model):
    id = models.AutoField(primary_key=True,auto_created=True)
    img = models.ImageField(upload_to="good_img/",verbose_name = 'Изображение товара',blank=True)
    tw = models.ForeignKey('good',on_delete=models.CASCADE,verbose_name = 'Товар',related_name='smthing_2')

class user_history(models.Model):
    id = models.AutoField(primary_key=True,auto_created=True)
    path = models.CharField(max_length=2000,verbose_name = 'url запроса')
    user = models.ForeignKey('auth.user',on_delete=models.SET_NULL,verbose_name = 'имя пользователя',related_name='reg_user',blank=True,null=True)
    time = models.DateTimeField(verbose_name = 'время действия')
    request_data = models.CharField(max_length=4000,verbose_name = 'тело запроса',blank=True,null=True)
    method = models.CharField(max_length=10000,verbose_name = 'метод запроса')
    agent = models.CharField(max_length=1000,verbose_name = 'агент запроса')