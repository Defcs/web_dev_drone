# Generated by Django 4.2.1 on 2023-06-07 17:15

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='catalog_first_lvl',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=200, verbose_name='Заголовок категории')),
                ('img', models.ImageField(blank=True, upload_to='catalog_first_lvl_img/', verbose_name='Изображение категории')),
                ('link', models.CharField(max_length=200, verbose_name='ТИП категории')),
            ],
            options={
                'verbose_name': 'Каталог первого уровня',
                'verbose_name_plural': 'Каталоги первого уровня',
            },
        ),
        migrations.CreateModel(
            name='catalog_sec_lvl',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=200, verbose_name='Заголовок категории')),
                ('img', models.ImageField(blank=True, upload_to='catalog_sec_lvl_img/', verbose_name='Изображение категории')),
                ('link', models.CharField(max_length=200, verbose_name='ТИП категории')),
                ('catalog_first', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='catalog_sec', to='main_functional.catalog_first_lvl', verbose_name='Каталог первого уровня')),
            ],
            options={
                'verbose_name': 'Каталог второго уровня',
                'verbose_name_plural': 'Каталоги второго уровня',
            },
        ),
        migrations.CreateModel(
            name='good',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=200, verbose_name='Заголовок товара')),
                ('desc', models.CharField(max_length=2000, verbose_name='Описание товара')),
                ('charact', models.CharField(max_length=2000, verbose_name='Характеристики товара')),
                ('count', models.CharField(max_length=5, verbose_name='Количество товара товара')),
                ('img', models.ImageField(blank=True, upload_to='good_img/', verbose_name='Изображение товара')),
                ('link', models.CharField(max_length=300, verbose_name='Полное наимаенование товара')),
                ('stars', models.CharField(max_length=1, verbose_name='Звезды')),
                ('new_price', models.CharField(max_length=7, verbose_name='Новая цена')),
                ('old_price', models.CharField(max_length=7, verbose_name='Старая цена')),
                ('material', models.CharField(max_length=5, verbose_name='Материал')),
                ('speed', models.CharField(max_length=5, verbose_name='Скорость')),
                ('fly_time', models.CharField(max_length=5, verbose_name='Время полета')),
                ('customer', models.ManyToManyField(blank=True, related_name='customer', to=settings.AUTH_USER_MODEL, verbose_name='Имя покупателя')),
                ('sec_lvl', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cat_sec', to='main_functional.catalog_sec_lvl', verbose_name='Каталог 2 лвлва')),
            ],
            options={
                'verbose_name': 'Товар',
                'verbose_name_plural': 'Товары',
            },
        ),
        migrations.CreateModel(
            name='reviews',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=200, verbose_name='Заголовок обзора')),
                ('desc', models.CharField(max_length=20000, verbose_name='Описание обзора')),
                ('stars', models.CharField(max_length=1, verbose_name='Звезды')),
                ('tw', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='smthing', to='main_functional.good', verbose_name='Товар')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reviewer', to=settings.AUTH_USER_MODEL, verbose_name='Имя обозревателя')),
            ],
            options={
                'verbose_name': 'Обзоры пользователя',
                'verbose_name_plural': 'Обзоры пользователей',
            },
        ),
        migrations.CreateModel(
            name='more_about_user',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False)),
                ('date_birth', models.DateTimeField(blank=True, verbose_name='Дата рождения')),
                ('gender', models.CharField(blank=True, max_length=200, verbose_name='Пол')),
                ('real_name', models.CharField(blank=True, max_length=200, verbose_name='Имя')),
                ('real_sename', models.CharField(blank=True, max_length=200, verbose_name='Фамилия')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='more_about_users', to=settings.AUTH_USER_MODEL, verbose_name='Имя пользователя')),
            ],
            options={
                'verbose_name': 'Дополнительно о пользователе',
                'verbose_name_plural': 'Дополнительно о пользователях',
            },
        ),
        migrations.CreateModel(
            name='comments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=200, verbose_name='Заголовок комментария')),
                ('desc', models.CharField(max_length=2000, verbose_name='Описание комментария')),
                ('stars', models.CharField(max_length=1, verbose_name='Звезды')),
                ('tw', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='smth', to='main_functional.good', verbose_name='Товар')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='commenter', to=settings.AUTH_USER_MODEL, verbose_name='Имя комментатора')),
            ],
            options={
                'verbose_name': 'Комментарий пользователя',
                'verbose_name_plural': 'Комментарии пользователей',
            },
        ),
    ]
