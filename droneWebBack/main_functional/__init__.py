# django_celery/__init__.py

from .celery_inf import app as celery_app

__all__ = ("celery_app",)

# No module named 'celery'