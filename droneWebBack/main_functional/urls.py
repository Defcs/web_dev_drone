from django.urls import include, path
from django.contrib.auth import update_session_auth_hash
from .models import more_about_user,catalog_first_lvl,catalog_sec_lvl,comments,reviews,good
from .resources import auth_resource
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from main_functional import views
from django.urls import re_path
from rest_framework_swagger.views import get_swagger_view
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view
from rest_framework import permissions

app_name = 'main_functional'




urlpatterns = [
    path('swagger-ui/', TemplateView.as_view(
        template_name='docs.html',
        extra_context={'schema_url':'api_schema'},
        ), name='swagger-ui'),

    path('registration', views.registrate),
    path('delete_user', views.delete_user),
    path('authorisation', auth_resource.CustomAuthToken.as_view()),
    path('update_user', views.update_user),
    path('catalog', views.get_catalog),
    path('', views.get_main_gage),
    path('catalog/<str:link>', views.get_sec_catalog),
    path('catalog/<str:link>/<str:link2>', views.get_goods),
    path('catalog/<str:link>/<str:link2>/<str:link3>', views.get_good),
    path('catalog/<str:link>/<str:link2>/<str:link3>/buy_good/<int:good_id>', views.buy_good),
    path('catalog/<str:link>/<str:link2>/<str:link3>/buy_good/<int:good_id>/imgs', views.get_good_img),
    path('add_comment', views.add_comment),
    path('delete_comment/<int:comment_id>', views.delete_comment),
    path('update_comment/<int:comment_id>', views.update_comment),
    path('catalog_mini', views.catalog_mini),
    # path('about us/', views.aboutUs, name='aboutUs'),
    # # path('registration', Auth_Controller.registration, name='registration'),
    
    # path('logout', Auth_Controller.logout_view, name='logout'),
    # path('registration', Auth_Controller.registration, name='registration'),
    # path('registrationpost', Auth_Controller.registrationpost, name='registrationpost'),
    # path('authorisation', Auth_Controller.authorisation, name='authorisation'),
    # path('authorisationpost', Auth_Controller.authorisationpost, name='authorisationpost'),
    # path('userdel', Auth_Controller.userdel, name='userdel'),

    # path('schedule', Schedule_Controller.schedule, name='schedule'),
    # path('schedule/del/<int:pk>', Schedule_Controller.schedule_del, name='schedule_del'),
    # path('schedule/<int:pk>', Schedule_Controller.schedule_more, name='schedule_more'),
    # path('schedule/add_comment/<int:pk>', Schedule_Controller.add_comment, name='add_comment'),
    # path('schedule/del_com/<int:pk>', Schedule_Controller.del_com, name='del_com'),
    # path('schedule/up_com/<int:pk>', Schedule_Controller.up_com, name='up_com'),
    # path('schedule/up_com/update_comment/<int:pk>', Schedule_Controller.update_com, name='update_com'),

    # path('history', History_Controller.get_information_about_model, name='history'),

    # path('export_Db_trainings', Export_Db_Controller.trainings_import, name='trainings'),
    
    
    # path('account/', views.account, name='account'),
    
    
    # path('', include('django.contrib.auth.urls')),
    # path('login/', views.login, name='login'),
    # path('location/', views.location, name='location'),
    
    # path('schedule/', views.schedul, name='schedul'),
    # path('account/my schedule/<int:users>', views.Myschedule, name='Myschedule'),
]