from rest_framework import status
from rest_framework.decorators import api_view
from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from django.contrib.auth.models import User
from main_functional.models import more_about_user
from main_functional.serializers import UserSerializer
from main_functional.serialisers.auth_serializers import User_dataSerializer,More_about_user_dataSerializer
from rest_framework import viewsets
from rest_framework import permissions
from django.contrib.auth import authenticate, login
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import random 
import string


from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_id': user.pk,
            'email': user.email
        })
# class UserViewSet(viewsets.ModelViewSet):
#     def get_user(user_id):
#         queryset = User.objects.filter(id=user_id).order_by('-date_joined')
#         serializer_class = UserSerializer


# @api_view(['GET', 'POST'])
# def registrate(request):
#     if request.method == 'POST':

#         # serializer = UserSerializer(data=request.data)
#         # serializer.save
#         username = request.POST['username']
#         password = request.POST['password']
#         email = request.POST['email']
#         first_name = request.POST['first_name']
#         last_name = request.POST['last_name']
#         date_birth= request.POST['bday']
#         user = User.objects.create_user(username,email,password)
#         # user = authenticate(request, username=username, password=password,is_superuser=0,is_staff=0,is_active=1)
#         # user.last_name = 'Lennon'
#         user.is_superuser='0'
#         user.is_staff='0'
#         user.first_name=first_name
#         user.last_name=last_name
#         user.save()
#         more_about = more_about_user()
#         more_about.user_id=user.id
#         # more_about.date_birth=date_birth
#         more_about.save()
#         send_feedback_email_task.delay(user.email, 'Вы зарегистрировались')
#         # serializer = UserSerializer(user)
#         # return JsonResponse(user)


# @csrf_exempt
# def authorisation(request):
#     if request.method == 'POST':
#         username = request.POST['username']
#         password = request.POST['password']
#         user = authenticate(request, username=username, password=password)
#         if user is not None:
#             token = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(12))
#             querysetUser = User.objects.filter(username=username).get()
#             querysetMoreAboutUser = more_about_user.objects.get(user__id=querysetUser.id)
#             querysetMoreAboutUser.token= token
#             querysetMoreAboutUser.save()
#             serializer = User_dataSerializer(querysetUser)
#             return JsonResponse(serializer.data)

@csrf_exempt
def delete_user(request):
    user_id = request.POST['id']
    token = request.POST['token']
    if request.method == 'POST' and Token.objects.get(user__id=user_id).key == token:
        querysetUser = User.objects.filter(id=user_id).get().delete()
        return JsonResponse({'response':'User has been deleted'})
    else: return HttpResponse(status=404)

@csrf_exempt
def update_user(request):
    user_id = request.POST['id']
    token = request.POST['token']
    more_about = more_about_user.objects.get(user__id=user_id)
    if request.method == 'POST' and more_about.token == token:
        date_birth= request.POST['bday']
        gender= request.POST['gender']
        name= request.POST['name']
        sname= request.POST['sname']
        more_about.date_birth=date_birth
        more_about.gender=gender
        more_about.real_name=name
        more_about.real_sename=sname
        try:
            more_about.save()
        except:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return JsonResponse({'response':'User has been updated'})
    else: return HttpResponse(status=404)

# from django.contrib.auth.models import User
# from main_functional.models import more_about_user
# from main_functional.serializers import UserSerializer
# from main_functional.serialisers.auth_serializers import User_dataSerializer,More_about_user_dataSerializer
# querysetUser = User.objects.filter(username='Vadim22').get()
# querysetMoreAboutUser = more_about_user.objects.get(user__id=querysetUser.id)
# serializer = User_dataSerializer(data=querysetUser)