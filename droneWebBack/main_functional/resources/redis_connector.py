"""Модуль работы с кэшэм"""
from django.conf import settings
from django.core.cache import cache
from django.contrib.auth.models import User
from django.core.cache.backends.base import DEFAULT_TIMEOUT

from main_functional.models import user_history

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

def put_into_casche(results, identical):
    """Функция кеширования"""
    cache.set(identical, results, timeout=CACHE_TTL)
    cache.set('id', identical, timeout=CACHE_TTL)
    print('cashe put', cache.get(identical))
    # print('cache keys', cache.keys())

def get_from_casche():
    """Функция получения информации из кэша"""
    data=[]
    identical = cache.get('id')
    while cache.get(identical) is not None:
        try:
            if (cache.get(identical).path == '/registrate' and
                User.objects.get(username = cache.get(identical).user)):
                i = cache.get(identical)
                i.user = User.objects.get(username = cache.get(identical).user)
                cache.set(identical, i, timeout=CACHE_TTL)
        except ImportError:
            print('get_casche error')
        print('cache in',cache.get(identical))
        data.append(cache.get(identical))
        identical-=1
    return {'data':data}

def save_cashe_into_db(datas='None'):
    """Функция сохранения кэша в бд"""
    for data in datas['data']:
        model = user_history()
        model.path = data['path']
        model.user = data['user']
        model.time = data['time']
        model.request_data = data['request_data']
        model.method = data['method']
        model.agent = data['agent']
        model.save()
    cache.clear()
