from django.shortcuts import render
from django.contrib.auth.models import User, Group
from .models import more_about_user,catalog_first_lvl,catalog_sec_lvl,comments,reviews,good,more_good_images
from rest_framework import viewsets
from rest_framework import permissions
from main_functional.serializers import UserSerializer, GroupSerializer, More_about_userSerializer, ReviewsSerialiser, CommentsSerialiser, GoodSerialiser, Catalog_sec_lvl_serialiser, CatalogSerialiser,Catalog_sec_lvl_serialiser_withoutgoods ,CommentsSerialiserWithGood ,CatalogSerialiser_mini,ImgSerialiser
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from main_functional.tasks import send_feedback_email_task
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated,AllowAny
from main_functional.serialisers.auth_serializers import User_dataSerializer,More_about_user_dataSerializer
from rest_framework import status
# from rest_framework.authentication import TokenAuthentication

# class UserViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows users to be viewed or edited.
#     """
#     queryset = User.objects.all().order_by('-date_joined')
#     serializer_class = UserSerializer
#     permission_classes = [permissions.IsAuthenticated]

# class CatalogViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows users to be viewed or edited.
#     """
#     queryset = catalog_first_lvl.objects.all().order_by('id')
#     serializer_class = CatalogSerialiser

# class CatalogSecViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows users to be viewed or edited.
#     """
#     queryset = catalog_sec_lvl.objects.all().order_by('id')
#     serializer_class = Catalog_sec_lvl_serialiser

# class GroupViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows groups to be viewed or edited.
#     """
#     queryset = Group.objects.all()
#     serializer_class = GroupSerializer
#     permission_classes = [permissions.IsAuthenticated]
@csrf_exempt
def get_main_gage(request):
    if request.method == 'GET':
        queryset = catalog_first_lvl.objects.all().order_by('id')[:4]
        serialiser = CatalogSerialiser(queryset,many=True)
        queryset = good.objects.filter(sec_lvl__link='copters')[:4]
        serialiser2 = Catalog_sec_lvl_serialiser_withoutgoods(queryset,many=True)
        return JsonResponse({'catalog':serialiser.data,'popular':serialiser2.data})
@csrf_exempt
def get_catalog(request):
    if request.method == 'GET':
        queryset = catalog_first_lvl.objects.all().order_by('id')
        serialiser = CatalogSerialiser(queryset,many=True)
        # serialiser.is_valid()
        print(serialiser.data)
        return JsonResponse({'catalog':serialiser.data,'path':['catalog']})

@csrf_exempt
def catalog_mini(request):
    if request.method == 'GET':
        queryset = catalog_first_lvl.objects.all().order_by('id')
        serialiser = CatalogSerialiser_mini(queryset,many=True)
        # serialiser.is_valid()
        print(serialiser.data)
        return JsonResponse({'catalog':serialiser.data})

@csrf_exempt
def get_sec_catalog(request,link):
    if request.method == 'GET':
        queryset = catalog_sec_lvl.objects.filter(catalog_first__link=link)
        serialiser = Catalog_sec_lvl_serialiser_withoutgoods(queryset,many=True)
        return JsonResponse({'catalog':serialiser.data,'path':['catalog',link],'title':queryset[0].catalog_first.title})

@csrf_exempt
def get_goods(request,link,link2):
    if request.method == 'GET':
        queryset = good.objects.filter(sec_lvl__link=link2)
        serialiser = GoodSerialiser(queryset,many=True)
        return JsonResponse({'catalog':serialiser.data,'path':['catalog',link,link2],'title':queryset[0].sec_lvl.title})

@csrf_exempt
def get_good(request,link,link2,link3):
    if request.method == 'GET':
        queryset = good.objects.filter(link=link3).get()
        serialiser = GoodSerialiser(queryset)
        return JsonResponse({'catalog':serialiser.data,'path':['catalog',link,link2,link3]})

@csrf_exempt
def get_good_img(request,link,link2,link3):
    if request.method == 'GET':
        queryset = more_good_images.objects.filter(tw__id = good.objects.filter(link=link3).get().id)
        serialiser = ImgSerialiser(queryset,many=True)
        return JsonResponse({'images':serialiser.data})

@api_view(['POST'])
@permission_classes([AllowAny])
def registrate(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']
        if username and password and email:
            user = User.objects.create_user(username,email,password)
            user.is_superuser='0'
            user.is_staff='0'
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']
            # date_birth = request.POST.get['b_day', False]
            # gender = request.POST['gender']
            if first_name:
                user.first_name=first_name
            if last_name:
                user.last_name=last_name
            user.save()
            more_about = more_about_user()
            more_about.user_id=user.id
            # if date_birth:
            #     more_about.date_birth=date_birth
            # if gender:
            #     more_about.gender=gender
            more_about.save()
            send_feedback_email_task.delay(user.email, 'Вы зарегистрировались')
            serializer = User_dataSerializer(user,many=False)
            print(user)
            print('fefefe')
            print(serializer.data,'fwfw')
            return JsonResponse({'request': serializer.data})

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def update_user(request):
    serializer = UserSerializer(data = request.data)
    serializer.is_valid()
    serializer.save()
    return JsonResponse(serializer.data)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def delete_user(request):
    request.user.delete()
    return JsonResponse({'response':'User has been deleted'})

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def update_user(request):
    serializer = UserSerializer(data = request.data)
    serializer.is_valid()
    serializer.save()
    return JsonResponse(serializer.data)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def buy_good(request,link,link2,link3,good_id):
    tovar = good.objects.get(id = good_id)
    tovar.customer.add(request.user)
    serialiser = GoodSerialiser(tovar)
    return JsonResponse({'catalog':serialiser.data,'path':['catalog',link,link2,link3]})

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def add_comment(request):
    if request.method == 'POST':
        comment = {
            'title':request.POST['title'],
            'desc':request.POST['desc'],
            'stars':request.POST['stars'],
            'user':User.objects.get(id = request.user.id).id,
            'tw':good.objects.get(id = request.POST['tw']).id,
            'user_id':User.objects.get(id = request.user.id).id,
        }
        serializer = CommentsSerialiserWithGood(data = comment)
        serializer.is_valid()
        serializer.save()
        return Response(status=status.HTTP_201_CREATED)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def update_comment(request,comment_id):
    if request.method == 'POST':
        comment = comments.objects.get(id=comment_id)
        comment.title = request.data['title']
        comment.title = request.data['desc']
        comment.title = request.data['stars']
        return Response(status=status.HTTP_201_UPDATED)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def delete_comment(request,comment_id):
    if request.user.is_superuser == 1 or comments.objects.get(id=comment_id).user.id:
        comments.objects.get(id=comment_id).delete()
        return JsonResponse({'response':'User has been deleted'})
