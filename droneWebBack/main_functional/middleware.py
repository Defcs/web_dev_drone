from drone_web_back.BlackList import BlackList
from drone_web_back.LogsHandler import logs_handler
from main_functional.tasks import redis_connector 
from django.core.cache import cache


class LogsMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        self.id = 0

    def __call__(self, request):
        if request.get_full_path() not in BlackList.return_black_list():
            results = logs_handler(request)
            print('results',results)
            if not results == None:
                redis_connector.put_into_casche(results,self.id)
            self.id+=1
            print('id', self.id)
        print(self.id-1,'casche po id',cache.get(self.id-1),'/',cache.get('id'))
        response = self.get_response(request)
        return response 