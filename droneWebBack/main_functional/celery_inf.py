# django_celery/celery.py

import os
from celery import Celery
from celery.schedules import crontab
from django.utils import timezone
from django.core.cache import cache
import redis
from django.conf import settings
# from main_functional.resources import redis_connector 

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "drone_web_back.settings")
app = Celery("main_functional")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()


# def get_from_casche():
#     """Функция получения информации из кэша"""
#     data=[]
#     identical = cache.get('id')
#     while not (cache.get(identical) == None):
#         try:
#             if (cache.get(identical).path == '/registrate' and
#                 User.objects.get(username = cache.get(identical).user)):
#                 i = cache.get(identical)
#                 i.user = User.objects.get(username = cache.get(identical).user).id
#                 cache.set(identical, i, timeout=CACHE_TTL)
#         except:
#             print('get_casche error')
#         print('cache in',cache.get(identical))
#         data.append(cache.get(identical))
#         identical-=1
#     return {'data':cache.get('id')}
# redis_instance = redis.StrictRedis(host=settings.REDIS_HOST,port=settings.REDIS_PORT, db=0)

app.conf.beat_schedule = {
    'saving_casche-message-1min': {
        'task': 'saving_casche',
        'schedule': crontab(minute='*/1'),
        # "args": redis_instance,
    },
} 


